# ARIA-MAUVE

This package provides MAUVE components and resources to interface with
Mobile Robots platforms using the libARIA communication library.

It is licensed under the [GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0).

## Instal Instructions

You first need to have installed the MAUVE [Toolchain](https://gitlab.com/mauve/mauve_toolchain).

To install the ARIA-MAUVE package in the MAUVE workspace:
```
cd ~/mauve_ws
git clone https://gitlab.com/MAUVE/aria_mauve.git src/aria_mauve
catkin_make
```

## Documentation

The AriaWrapper class defines a generic wrapper of libARIA, that is then used
both in the component AriaComponent and the resource AriaResource.

To interact with the robot, you can use the component or the resource.

### AriaComponent

The AriaComponent has a PeriodicStateMachine, and provides as interface:

* an output port _pose_ with the 2D pose of the robot coming from odometry
* an output port _velocity_ with the current 2D velocity of the robot
* an output port _voltage_ with the current battery voltage
* an output port _time_ with the current time of the robot controller
* an input port *velocity_command* with the 2D velocity to apply to the robot

### AriaResource

The AriaResource provides the following interface:

* a _stop_ event to stop the robot motion
* a *read_location* service to read the current location (_x_ and _y_ coordinates) computed according to the robot odometry
* a *read_pose* service to read the current 2D pose (from odometry)
* a *read_velocity* service to read the current velocity
* a *read_heading* service to read the current heading (from odometry)
* a *read_battery* service to read the battery voltage
* a *read_time* service to read the robot controller time
* a *write_velocity* service to write a velocity command

### Testing

To test the connection with the robot:
```
rosrun aria_mauve aria_mauve_component
```
