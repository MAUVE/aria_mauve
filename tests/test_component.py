#!/usr/bin/ipython -i
#####
# Copyright 2018 ONERA
#
# This file is part of the MAUVE ARIA project.
#
# MAUVE ARIA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# MAUVE ARIA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
import mauve_runtime
mauve_runtime.load_deployer("devel/lib/libmauve_aria_component.so")

mauve_runtime.initialize_logger(b'''
default:
  level: debug
  type: stdout
''')

deployer = mauve_runtime.deployer
deployer.architecture.configure()
deployer.create_tasks()
deployer.activate()
deployer.start()
