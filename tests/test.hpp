/* Copyright 2018 ONERA
 *
 * This file is part of the MAUVE ARIA project.
 *
 * MAUVE ARIA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ARIA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "aria/mauve/AriaComponent.hpp"
#include "aria/mauve/AriaResource.hpp"

namespace aria {
namespace mauve {

struct AriaComponentArchitecture : public ::mauve::runtime::Architecture {
  AriaComponent & aria_comp = mk_component<AriaComponent>("robot");

  ::mauve::runtime::SharedData<::mauve::types::geometry::Pose2D> & pose =
    mk_resource< ::mauve::runtime::SharedData<::mauve::types::geometry::Pose2D> >("pose", ::mauve::types::geometry::Pose2D());
  ::mauve::runtime::SharedData<::mauve::types::geometry::UnicycleVelocity> & velocity =
    mk_resource< ::mauve::runtime::SharedData<::mauve::types::geometry::UnicycleVelocity> >("velocity", ::mauve::types::geometry::UnicycleVelocity());
  ::mauve::runtime::SharedData<double> & battery =
    mk_resource< ::mauve::runtime::SharedData<double> >("battery", 0);
  ::mauve::runtime::SharedData<double> & time = mk_resource< ::mauve::runtime::SharedData<double> >("time", 0);
  ::mauve::runtime::SharedData<::mauve::types::geometry::UnicycleVelocity> & command =
    mk_resource< ::mauve::runtime::SharedData<::mauve::types::geometry::UnicycleVelocity> >("command", ::mauve::types::geometry::UnicycleVelocity());

  bool configure_hook() override {
    aria_comp.shell().pose.connect(pose.interface().write);
    aria_comp.shell().velocity.connect(velocity.interface().write);
    aria_comp.shell().voltage.connect(battery.interface().write);
    aria_comp.shell().vel_cmd.connect(command.interface().read);
    aria_comp.shell().time.connect(time.interface().write);

    return ::mauve::runtime::Architecture::configure_hook();
  };
};

struct AriaResourceArchitecture : public ::mauve::runtime::Architecture {
  AriaResource & aria_res = mk_resource<AriaResource>("robot");
};

}}
